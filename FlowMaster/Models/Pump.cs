﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Flowmaster.Constants;

namespace Flowmaster.Models
{
    /// <summary>
    /// This is a pump that enables waterflow
    /// </summary>
    public class Pump : FlowComponent
    {
        public bool IsOn { get; set; }

        public Pump(double x, double y) : base(x, y, "Assets/pump.png")
        {
            IsOn = false;
        }

        /// <summary>
        /// toggles the pump between on or off
        /// </summary>
        public void ToggleState()
        {
            IsOn = !IsOn;
            ComponentImage.Source = new BitmapImage(new Uri(IsOn ? "Assets/pump_on.png" : "Assets/pump.png", UriKind.Relative));
        }
    }

}
