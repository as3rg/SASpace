﻿using Flowmaster.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace Flowmaster.Models
{  
    /// <summary>
    /// Parent class with all the common features and variables for Pump, Valve and mManifold classes
    /// </summary>
    public abstract class FlowComponent
    {
        /// <summary>
        /// Уникальный идентификатор объекта для восстановления после десериализации
        /// </summary>
        public Guid ID { get; set; } = Guid.NewGuid();

        public double X { get; set; }
        public double Y { get; set; }

        /// <summary>
        /// Объекты графического интерфейса не подлежат сериализации
        /// </summary>
        [JsonIgnore]
        public Image ComponentImage { get; set; }
        
        /// <summary>
        /// Every time a component object is created, it has its own list of connectedPipes. 
        /// When a pipe is created (that's connected to this Component), a method is invoked that adds that pipe to this Component
        /// Так как трубопровод в себе содержит информацию о соединяемых компонентах, этот список для сериализации не нужен
        /// </summary>
        [JsonIgnore]
        // инициализация списка по умолчанию
        public List<Pipe> ConnectedPipes { get; set; } = new List<Pipe>();

        public FlowComponent(double x, double y, string imagePath)
        {
            X = x;
            Y = y;
            ComponentImage = new Image
            {
                Source = new BitmapImage(new Uri(imagePath, UriKind.Relative)),
                Width = ProjectConstants.ImageWidth,
                Height = ProjectConstants.ImageHeight,
                Tag = this
            };
            ConnectedPipes = new List<Pipe>();
        }
        
        /// <summary>
        /// method for adding pipes (connected to this object) to the list of connected pipes
        /// </summary>
        /// <param name="pipe"></param>
        public void AddConnectedPipe (Pipe pipe)
        {
            ConnectedPipes.Add(pipe);
        }

        /// <summary>
        /// This method is used in deserialization, it adds ComponentImage to the deserialized object and restores the Tag property
        /// </summary>        
        public void CreateComponentImage(string imagePath)
        {
            ComponentImage = new Image
            {
                Source = new BitmapImage(new Uri(imagePath, UriKind.Relative)),
                Width = ProjectConstants.ImageWidth,
                Height = ProjectConstants.ImageHeight,
                Tag = this
            };
        }
    }
}
