﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Flowmaster.Constants;

namespace Flowmaster.Models
{
    /// <summary>
    /// This object stops the waterflow
    /// </summary>
   public class Valve : FlowComponent
    {
        public bool IsOpen { get; set; }    
        public Valve(double x, double y) : base(x, y, "Assets/valve.png")
        {
            IsOpen = false;
        }
        /// <summary>
        /// toggles the valve between on or off
        /// </summary>
        public void ToggleState() 
        {
            IsOpen = !IsOpen;
            ComponentImage.Source = new BitmapImage(new Uri(IsOpen ? "Assets/valve_open.png" : "Assets/valve.png", UriKind.Relative));
        }
    }
}
