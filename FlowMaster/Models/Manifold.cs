﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Flowmaster.Constants;

namespace Flowmaster.Models
{
    /// <summary>
    /// Manifold is a node that connects several edges (pipelines) in a single spot
    /// </summary>
    public class Manifold : FlowComponent
    {
        public Manifold(double x, double y) : base(x, y, "Assets/manifold.png")
        {
        }
    }
}
