﻿using Flowmaster.Constants;
using Newtonsoft.Json;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Flowmaster.Models
{
    public class Pipe
    {
        public FlowComponent firstComponent;
        public FlowComponent secondComponent;

        /// <summary>
        /// Графические компоненты не сериализируются
        /// </summary>
        [JsonIgnore]
        public Line outline;

        /// <summary>
        /// Графические компоненты не сериализируются
        /// </summary>
        [JsonIgnore]
        public Line innerline;
        
        /// <summary>
        /// Constructor. Added a call to method that adds this pipe to the corresponding object
        /// </summary>
        public Pipe(FlowComponent firstComponent, FlowComponent secondComponent)
        {
            this.firstComponent = firstComponent;
            this.secondComponent = secondComponent;
            
            // I put this check, because constructor can be called with 1 normal argument and 1 null argument - look at tempPipe.
            // tempPipe doesn't live long and hence this "half-pipe" shouldn't be added to the list of connected Pipes.
            if (firstComponent!= null && secondComponent != null)
            {
                firstComponent.AddConnectedPipe(this);
                secondComponent.AddConnectedPipe(this);
            }
        }  
        public void AddToCanvas(Canvas plottingArea)
        {
            // create Outline
            outline = new Line
            {
                Stroke = Brushes.Black,
                StrokeThickness = 12, // thickness of the outline. I'll find the best value later
                X1 = firstComponent.X + (ProjectConstants.ImageWidth / 2),
                Y1 = firstComponent.Y + (ProjectConstants.ImageHeight / 2),
                X2 = secondComponent.X + (ProjectConstants.ImageWidth / 2),
                Y2 = secondComponent.Y + (ProjectConstants.ImageHeight / 2),
                
            };
            outline.Tag = this; // I'll need this for deleting objects (pipes) feature
            // create innerline (blue or gray in color depending whether there is water in it)
            innerline = new Line
            {
                Stroke = Brushes.LightGray,  // I'll need a tumbler here later on. 
                StrokeThickness = 10,
                X1 = firstComponent.X + (ProjectConstants.ImageWidth / 2),
                Y1 = firstComponent.Y + (ProjectConstants.ImageHeight / 2),
                X2 = secondComponent.X + (ProjectConstants.ImageWidth / 2),
                Y2 = secondComponent.Y + (ProjectConstants.ImageHeight / 2)
            };
            innerline.Tag = this; // I'll need this for deleting objects (pipes) feature
            // Elements positioning. pipes will be above grid but below objects (pipes, valves, manifolds)
            outline.SetValue(Canvas.ZIndexProperty, 1);
            innerline.SetValue(Canvas.ZIndexProperty, 2);
            plottingArea.Children.Add(outline);
            plottingArea.Children.Add(innerline);
        }

        /// <summary>
        /// Redraws pipe lines after a node was moved by updating the positions of the lines. It's called from MouseMove 
        /// </summary>
        public void UpdatePositions()
        {
            outline.X1 = firstComponent.X + (ProjectConstants.ImageWidth / 2);
            outline.Y1 = firstComponent.Y + (ProjectConstants.ImageHeight / 2);
            outline.X2 = secondComponent.X + (ProjectConstants.ImageWidth / 2);
            outline.Y2 = secondComponent.Y + (ProjectConstants.ImageHeight / 2);

            innerline.X1 = firstComponent.X + (ProjectConstants.ImageWidth / 2);
            innerline.Y1 = firstComponent.Y + (ProjectConstants.ImageHeight / 2);
            innerline.X2 = secondComponent.X + (ProjectConstants.ImageWidth / 2);
            innerline.Y2 = secondComponent.Y + (ProjectConstants.ImageHeight / 2);
        }
    }
}
