﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flowmaster.Models
{
    /// <summary>
    /// PipeIDs is a no-frills copy of Pipe class. It's used in deserialization. It holds the info about 2 components - their ids.
    /// </summary>
    public class PipeIDs
    {
        public Guid FirstComponentID { get; set; }
        public Guid SecondComponentID { get; set;}

        public PipeIDs(Guid id1, Guid id2) 
        {
            FirstComponentID = id1;
            SecondComponentID = id2;
        }
    }
}
