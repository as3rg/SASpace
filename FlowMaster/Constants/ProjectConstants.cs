﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;

namespace Flowmaster.Constants
{
    public static class ProjectConstants
    {
        
        public static double ImageWidth => 50;
        
        public static double ImageHeight => 50;

        public static int GridSize => 25;
    }
}