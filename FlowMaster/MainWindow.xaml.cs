﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flowmaster.Constants;
using Flowmaster.Models;
using System.IO;
using Newtonsoft.Json;

namespace Flowmaster
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// List of objects (pumps, valves, manifolds) created during the drawing cycle
        /// </summary>
        private List<FlowComponent> components = new List<FlowComponent>();
        private List<Pump> pumps = new List<Pump>();
        private List<Valve> valves = new List<Valve>();
        private List<Manifold> manifolds = new List<Manifold>();
        private List<Pipe> pipes = new List<Pipe>();

        private double x; // a coordinate of the mouse cursor (and later objects) is recorded here
        private double y; // a coordinate of the mouse cursor (and later objects) is recorded here

        /// <summary>
        /// Keeps track of the current pipe that is being installed. It's a temp variable that's erased at the end of each iteraction
        /// </summary>
        private Pipe tempPipe;

        public MainWindow()
        {
            InitializeComponent();

            //start in Fullscreen
            this.WindowState = WindowState.Maximized;
            this.WindowStyle = WindowStyle.None;
        }

        /// <summary>
        /// DrawCoordinateSystem is invoked right after startup via Loaded event to draw a grid: vertical and horizontal lines are created in 2 cycles. 
        /// </summary>
        private void DrawCoordinateSystem()
        {
            int gridSize = ProjectConstants.GridSize; // Gridsize = 25

            // Draw ordinates
            for (int i = 0; i <= PlottingArea.ActualWidth; i += gridSize)
            {
                Line verticalLine = new Line
                {
                    X1 = i,
                    Y1 = 0,
                    X2 = i,
                    Y2 = PlottingArea.ActualHeight,
                    Stroke = Brushes.LightGray,
                    StrokeThickness = 0.5
                };
                // Elements positioning. Grid will be drawn at the lowest level "below" pipes and objects (ZIndexProperty)
                verticalLine.SetValue(Canvas.ZIndexProperty, 0);
                PlottingArea.Children.Add(verticalLine);
            }
            // Draw abscissa
            for (int i = 0; i <= PlottingArea.ActualHeight; i += gridSize)
            {
                Line horizontalLine = new Line
                {
                    X1 = 0,
                    Y1 = i,
                    X2 = PlottingArea.ActualWidth,
                    Y2 = i,
                    Stroke = Brushes.LightGray,
                    StrokeThickness = 0.5
                };
                // Elements positioning. Grid will be drawn at the lowest level (beloew pipes and objects)
                horizontalLine.SetValue(Canvas.ZIndexProperty, 0);
                PlottingArea.Children.Add(horizontalLine);
            }
        }

        /// <summary>
        /// These 3 methods (pumpbutton_click, valvebutton_click and manifoldbutton_click) are event handlers. 
        /// When a button in the toolbar is clicked, one of them is invoked. A corresponding object is created in the next method (AddFlowComponent)
        /// and the next method for drag-n-dropping is called with a newly-created object as an argument.
        /// </summary>
        private void PumpButton_Click(object sender, EventArgs e)
        {
            pumps.Add(new Pump(0, 0)); // add newly created object to the list
            AddFlowComponent(pumps.Last());  //
        }
        private void ValveButton_Click(object sender, EventArgs e)
        {
            valves.Add(new Valve(0, 0)); // add newly created object to the list
            AddFlowComponent(valves.Last());
        }
        private void ManifoldButton_Click(object sender, EventArgs e)
        {
            manifolds.Add(new Manifold(0, 0));  // add newly created object to the list
            AddFlowComponent(manifolds.Last());
        }

        /// <summary>
        /// Event handler when the pipe button was clicked. It activates pipeSettingMode that allows to connect 2 objects with a pipe
        /// </summary>        
        private void PipeButton_Click(object sender, EventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Cross;
        }

        /// <summary>
        /// Event handler to start the FlowMaster (show the waterflow and color the corresponding pipes and manifolds)
        /// </summary>
        private void PlayButton_Click(object sender, EventArgs e)
        {
            foreach (Pump pump in pumps)
            {
                // Check if the pump has any connected pipes. If not, simply skip this iteration and move on
                if (pump.ConnectedPipes.Count == 0)
                {
                    continue;
                }

                Pipe connectedPipe = pump.ConnectedPipes[0]; // I can write so, because we agreed that pumps can have only 1 pipe!

                if (pump.IsOn && connectedPipe.innerline.Stroke != Brushes.Blue) // Second condition verifies that this node wasn't "traveled" before. Unexplorerd pumps couldn't have a single connecting pipe colored blue. If the pump is unexplorerd then it belongs to a separate unconnected network. 
                {
                    BuildFlowDiagram(pump);
                }
            }
        }

        /// <summary>
        /// Resets all pipes and manifold nodes to gray
        /// </summary>        
        private void Reset_Click(object sender, EventArgs e)
        {
            // resets the color of all pipes to gray
            foreach (Pipe pipe in pipes)
            {
                pipe.innerline.Stroke = Brushes.LightGray;
            }

            // resets the image of all manifolds to the gray manifold
            foreach (Manifold manifold in manifolds)
            {
                if (manifold.ComponentImage.Source.ToString() != "Assets/manifold_gray.png") // check that manifold isn't grey already
                {
                    manifold.ComponentImage.Source = new BitmapImage(new Uri("Assets/manifold.png", UriKind.Relative));
                }
            }
        }

        /// <summary>
        /// activates Delete mode by changing the cursor.
        /// </summary>
        private void Delete_Click(object sender, EventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Pen;
        }


        /// <summary>
        /// Clears all objects
        /// </summary>        
        private void NewButton_Click(object sender, EventArgs e)
        {
            // Clear all lists with corresponding objects
            components.Clear();
            pumps.Clear();
            valves.Clear();
            manifolds.Clear();
            pipes.Clear();

            // Remove all objects on the PlottingArea
            PlottingArea.Children.Clear();
            DrawCoordinateSystem();
        }


        /// <summary>
        /// AddFlowComponent places the object (acccording to  the clicked button) onto the Plotting Area.
        /// The object is placed both "physicaly" (component is created, coordinates are updated) and "graphically" 
        /// in the top left corner. The object is also added to the components list (!)
        /// </summary>        
        private void AddFlowComponent(FlowComponent component)
        {
            component.ComponentImage.SetValue(Canvas.ZIndexProperty, 3);
            PlottingArea.Children.Add(component.ComponentImage);
            Canvas.SetLeft(component.ComponentImage, component.X);
            Canvas.SetTop(component.ComponentImage, component.Y);
            components.Add(component);

            component.ComponentImage.MouseDown += ComponentImage_MouseDown;
            component.ComponentImage.MouseUp += ComponentImage_MouseUp;
            component.ComponentImage.MouseMove += ComponentImage_MouseMove;
            component.ComponentImage.MouseRightButtonUp += ComponentImage_MouseRightButtonUp;
        }

        /// <summary>
        /// What happens when object has been clicked. Note that this event handler is created individually for each particular object (valve, button, manifold)
        /// We always know objects was clicked due to the nature of the event handler. Pipes can also be clicked when in the Delete mode. 
        /// </summary>
        private void ComponentImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Image image && Mouse.OverrideCursor != Cursors.Cross)
            {
                // get the coordinates
                x = e.GetPosition(image).X;
                y = e.GetPosition(image).Y;
            }

            // this part of the code refers to Pipesetting mode. Pipe-symbol was clicked (cursor is Cross).
            if (Mouse.OverrideCursor == Cursors.Cross)
            {
                // 1) (Image)sender: cast sender to an Image type; 2) retrieve the Tag; 3) Cast the result to FLowComponent type 
                FlowComponent clickedComponent = (FlowComponent)((Image)sender).Tag;

                // nothing was clicked. Try to get here, I think this code is redundant
                if (clickedComponent == null)
                {
                    // setting-pipe is over. null everything. 
                    tempPipe = null;
                    Mouse.OverrideCursor = null;
                    return;
                }

                // This check is VERY important - it PREVENTS hooking two pipes to a pump. It's essential for BuildFlowDiagram
                // If clickedComponent is a pump and its ConnectedPipes list is not empty, then exit
                if (clickedComponent is Pump pump && pump.ConnectedPipes.Count > 0)
                {
                    tempPipe = null;
                    Mouse.OverrideCursor = null;
                    return;
                }

                // This check PREVENTS a VALVE to have more than 2 PIPES. The program will work without it, but it's a question of rules that we agreed on before.
                if (clickedComponent is Valve valve && valve.ConnectedPipes.Count > 1)
                {
                    tempPipe = null;
                    Mouse.OverrideCursor = null;
                    return;
                }

                // User clicked a second object (firstComponent is alread set) and it's the firstComponent AGAIN => Logic error
                if (clickedComponent == tempPipe?.firstComponent)
                {
                    // setting-pipe is over. null everything. 
                    tempPipe = null;
                    Mouse.OverrideCursor = null;
                    return;
                }

                // User clicked the first object after the pipe-setting had begun. Call the constructor 
                if (tempPipe == null)
                {
                    tempPipe = new Pipe(clickedComponent, null);
                }

                // We clicked the second object after the pipe-setting had begun. This is the final part of installing a pipe
                else
                {
                    // write down the clicked object
                    tempPipe.secondComponent = clickedComponent;

                    // Call a constructor (again, for the 2nd time!) to create a pipe object with a beginning and an end of the pipe. 
                    Pipe newPipe = new Pipe(tempPipe.firstComponent, tempPipe.secondComponent);

                    // create lines and draw them
                    newPipe.AddToCanvas(PlottingArea);

                    // add pipe to List for future reference
                    pipes.Add(newPipe);

                    // create event handlers when a pipe is clicked. This is required for the Delete mode when we want to delete a pipe (we need to click it)! Actually we click a line (inner or outerline) - it's a graphical representation of a pipe.
                    newPipe.outline.MouseDown += PipeLine_MouseDown;
                    newPipe.innerline.MouseDown += PipeLine_MouseDown;

                    // setting-pipe is over. null everything. 
                    tempPipe = null;
                    newPipe = null;
                    Mouse.OverrideCursor = null;
                }
            }

            // Deleting pumps, valves and manifolds when they are clicked in Delete mode
            if (Mouse.OverrideCursor == Cursors.Pen)
            {
                // Cast clicked image to FLowComponent type 
                FlowComponent clickedComponent = (FlowComponent)((Image)sender).Tag;

                // Check that an actual object was clicked
                if (clickedComponent != null)
                {
                    Guid clickedComponentID = clickedComponent.ID;

                    // Delete  graphics ( lines) from  the PlottingArea that exist in the connectedPipes list
                    foreach (Pipe pipe in clickedComponent.ConnectedPipes)
                    {
                        PlottingArea.Children.Remove(pipe.innerline); // remove graphics (lines) from  the PlottingArea
                        PlottingArea.Children.Remove(pipe.outline);
                    }

                    // Delete connected pipes from other components
                    foreach (FlowComponent component in components)
                    {
                        component.ConnectedPipes.RemoveAll(pipe => pipe.firstComponent.ID == clickedComponentID || pipe.secondComponent.ID == clickedComponentID);
                    }

                    // Delete pipes from the pipes list
                    pipes.RemoveAll(pipe => pipe.firstComponent.ID == clickedComponentID || pipe.secondComponent.ID == clickedComponentID);

                    // Delete object from the components list, and pumps, valves, and manifolds lists if applicable
                    components.RemoveAll(component => component.ID == clickedComponentID);
                    pumps.RemoveAll(pump => pump.ID == clickedComponentID);
                    valves.RemoveAll(valve => valve.ID == clickedComponentID);
                    manifolds.RemoveAll(manifold => manifold.ID == clickedComponentID);

                    // Remove object graphics from the PlottingArea
                    PlottingArea.Children.Remove((Image)sender);
                    // exit Delete mode
                    Mouse.OverrideCursor = null;

                    return;
                }
            }
        }

        /// <summary>
        /// This method gets invoked when user clicks a pipe with the intention to... erase this pipe (from all the lists and from Canvas)
        /// </summary>
        public void PipeLine_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.OverrideCursor == Cursors.Pen)
            {
                // sender object is supposed to be of the Line type               
                var line = sender as Line;

                // check that cast was successful
                if (line != null)
                {
                    // access Pipe object via Line's Tag property.
                    Pipe thisPipe = line.Tag as Pipe;

                    // Delete everything!
                    PlottingArea.Children.Remove(thisPipe.innerline); // remove graphics (lines) from  the PlottingArea
                    PlottingArea.Children.Remove(thisPipe.outline);
                    pipes.Remove(thisPipe); // remove object altogether

                    // Remove the pipe from the connected components' lists
                    if (thisPipe.firstComponent != null)
                    {
                        thisPipe.firstComponent.ConnectedPipes.Remove(thisPipe);
                    }
                    if (thisPipe.secondComponent != null)
                    {
                        thisPipe.secondComponent.ConnectedPipes.Remove(thisPipe);
                    }

                    // exit Delete mode
                    Mouse.OverrideCursor = null;
                }
            }
        }

        /// <summary>
        /// What happens when mouse moves when after the object was clicked and is kept pressed
        /// </summary>
        private void ComponentImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && sender is Image image && Mouse.OverrideCursor != Cursors.Cross)
            {
                Canvas.SetLeft((UIElement)sender, e.GetPosition(PlottingArea).X - x);
                Canvas.SetTop((UIElement)sender, e.GetPosition(PlottingArea).Y - y);

                // Update object's X and Y coordinates in real-time for scenarios when mouse is suddenly jerked (images "falls" on Canvas without MouseUp)
                FlowComponent clickedComponent = (FlowComponent)((Image)sender).Tag;
                clickedComponent.X = e.GetPosition(PlottingArea).X - x;
                clickedComponent.Y = e.GetPosition(PlottingArea).Y - y;

                // тащить за собой трубопроводы
                foreach (Pipe connectedPipe in clickedComponent.ConnectedPipes)
                {
                    connectedPipe.UpdatePositions();
                }
            }
        }

        /// <summary>
        /// The mouse left button is released (after it dragged the object)
        /// </summary>
        private void ComponentImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is Image image)
            {
                x = Math.Round(Canvas.GetLeft(image) / ProjectConstants.GridSize) * ProjectConstants.GridSize; //this line (and next line) transforms coordinates into x,y = 25 or 50 or 75 etc
                y = Math.Round(Canvas.GetTop(image) / ProjectConstants.GridSize) * ProjectConstants.GridSize;
                Canvas.SetLeft(image, x);
                Canvas.SetTop(image, y);
                // вернули координаты в объект. 
                if (image.Tag is FlowComponent component)
                {
                    component.X = x;
                    component.Y = y;
                }
            }
        }

        /// <summary>
        /// Toggling pumps and valves (by right-clicking them!)
        /// </summary>
        private void ComponentImage_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.OverrideCursor != Cursors.Cross) // On the second though, maybe it's redundant
            {
                // capture an object under Click
                FlowComponent clickedComponent = (FlowComponent)((Image)sender).Tag;
                
                // If there was an object:
                if (clickedComponent != null)
                {
                    if (clickedComponent is Pump pump)
                    {
                        pump.ToggleState(); // Turn on/off the pump
                    }
                    else if (clickedComponent is Valve valve)
                    {
                        valve.ToggleState(); // Turn on/off the valve
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// THIS IS THE CORE of the Flowmaster. It's a recursive function that provides a solution to the problem. 
        /// It follows the connected pipe and changes its color. Then it moves on to the next component. 
        /// If the next component is a closed valve, the iteration is over. If not, then a new instance of this function of invoked. 
        /// </summary>
        /// <param name="component"></param>
        private void BuildFlowDiagram(FlowComponent component)
        {
            // The function was invoked with a closed valve as an argument => end this iteration
            if (component is Valve valve && !valve.IsOpen)
            {
                return;
            }

            if (component is Manifold manifold) // Check whether this manifold is already conducting water to avoid unnecessary step
            {
                component.ComponentImage.Source = new BitmapImage(new Uri("Assets/manifold_wet.png", UriKind.Relative));
            }

            // Go over all the pipes connected to the current node
            foreach (Pipe connectedPipe in component.ConnectedPipes)
            {
                // Check if the pipe has already been colored blue
                if (connectedPipe.innerline.Stroke == Brushes.Blue)
                {
                    // Skip this iteration and move on to the next pipe
                    continue;
                }
                //color each pipe
                connectedPipe.innerline.Stroke = Brushes.Blue;

                // select a node on the side of the current pipe
                FlowComponent nextComponent = connectedPipe.firstComponent == component ? connectedPipe.secondComponent : connectedPipe.firstComponent;

                // run another iteration of this recursion
                BuildFlowDiagram(nextComponent);
            }
        }

        /// <summary>
        /// Window_Loaded event handler is invoked when Loaded event of the Window object happens. It's a neat trick that Sergey showed me.
        /// Window_Loaded invokes DrawCoordinateSystem to draw a grid. 
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //draw Grid
            DrawCoordinateSystem();
        }

        /// <summary>
        /// Serialization of all objects: pumps, valves, manifolds and pipes
        /// </summary>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            // straighforward serialization. I learned my lesson ;)
            SerializeThisList(pumps, "pumps.json");
            SerializeThisList(valves, "valves.json");
            SerializeThisList(manifolds, "manifolds.json");

            // It's really hard to deserialize a list of pipes due to the nature of the pipe object.
            // Instead we make a simple list of PipeIDs (just 2 GUIDs and nothing else)
            List<PipeIDs> pipeIDsList = new List<PipeIDs>();
            foreach (Pipe pipe in pipes)
            {
                PipeIDs pipeIds = new PipeIDs(pipe.firstComponent.ID, pipe.secondComponent.ID);
                pipeIDsList.Add(pipeIds);
            }

            // straighforward serialization of pipeIDs lsit. I learned my lesson, again! ;)
            SerializeThisList(pipeIDsList, "pipes.json");

            MessageBox.Show("Сохранение успешно завершено! При сохранении все объекты были сброшены в состояние по умолчанию.");
        }

        /// <summary>
        /// Deserialization: loading all the saved objects onto the PlottingArea
        /// </summary>
        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            // clear the Grid (graphically) and 2 lists. Other lists (pumps, valves, manifolds) are erased inside Deserialize method.
            PlottingArea.Children.Clear();
            DrawCoordinateSystem();
            pipes.Clear();
            components.Clear();

            // These 3 lists (pumps, valves, manifolds) are pretty straightforward to deserialize. Template methods in action
            DeserializeThisList(pumps, "pumps.json");
            DeserializeThisList(valves, "valves.json");
            DeserializeThisList(manifolds, "manifolds.json");

            // CreateComponentImage adds Image and Tag property for each object - without them object isn't complete
            // Then we invoke AddFlowComponent for each object: a drawing method + button-binding method from the beginning
            foreach (var pump in pumps)
            {
                pump.CreateComponentImage("Assets/pump.png");
                pump.IsOn = false;
                AddFlowComponent(pump);
            }
            
            foreach (var valve in valves) // read line 534
            {
                valve.CreateComponentImage("Assets/valve.png");
                valve.IsOpen = false;
                AddFlowComponent(valve);
            }

            foreach (var manifold in manifolds) // read line 534
            {
                manifold.CreateComponentImage("Assets/manifold.png");
                AddFlowComponent(manifold);
            }

            // List of PipeIDs object is required for recreating pipes. It stores objects consisting of 2 GUIDs
            List<PipeIDs> pipeIDsList = new List<PipeIDs>();
            
            // Pipes deserialization is a bit tricky. I deserialize it as a list of PipeIDs objects,
            // then I find corresponding components and step 3 - create new Pipe objects
            DeserializeThisList(pipeIDsList, "pipes.json");

            // Creating pipes by looking up IDs of components
            foreach (var pipeIDs in pipeIDsList)
            {
                // find components
                FlowComponent firstComponent = FindComponentByID(pipeIDs.FirstComponentID);
                FlowComponent secondComponent = FindComponentByID(pipeIDs.SecondComponentID);

                // create a pipe, add it to the pipes list and draw it.
                if (firstComponent != null && secondComponent != null)
                {
                    Pipe pipe = new Pipe(firstComponent, secondComponent);
                    pipes.Add(pipe);
                    pipe.AddToCanvas(PlottingArea);

                    // create event handlers when pipe is clicked (will be used for deleting pipes)
                    pipe.outline.MouseDown += PipeLine_MouseDown;
                    pipe.innerline.MouseDown += PipeLine_MouseDown;
                }
            }
        }

        /// <summary>
        /// Basic serialization of a list
        /// </summary>
        private void SerializeThisList<T>(List<T> list, string fileName)
        {
            var json = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText(fileName, json);
        }

        /// <summary>
        /// Deserialization and (!) adding the object to the corresponding list (pumps, valves etc)
        /// </summary>
        private void DeserializeThisList<T>(List<T> list, string fileName)
        {
            list.Clear();
            var json = File.ReadAllText(fileName);
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
            };

            var deserializedList = JsonConvert.DeserializeObject<List<T>>(json, settings);
            list.AddRange(deserializedList);
        }

        /// <summary>
        /// Method is used in load-button Click as part of the Pipe Deserialization process. It searches for a component with a particular id in the list of components 
        /// </summary>
        private FlowComponent FindComponentByID(Guid id)
        {
            FlowComponent component = components.Find(p => p.ID == id);
            return component;
        }

    }
}

